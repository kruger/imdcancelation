# Intermodulation distortion cancelation

A fast method to digitally cancel third order intermodulation distortion of strong RFI signals, thereby improving the spurious-free dynamic range of an RF system
- IMD are substracted from raw data
- No knowledge of RFI is needed, but it is assumed that the RFI is much stronger than the noise level.
- Distortions are assumed to have a constant phase e.g. for 3rd order distortion the 4th order polyspectra (between 3 input frequencies and distortion frequency) should have the same phase.

# Ideal
1. Consider the k-th frequency band <img src="https://render.githubusercontent.com/render/math?math=f_k=F(x)_k"> where _F_ is a perfect reconstructable polyphase filter and _x_ the raw data.
1. Set the frequency band to zero: <img src="https://render.githubusercontent.com/render/math?math=x_k = x - F^{-1} (f_k)"> 
1. Calculate the intermodulation distortion that fall into the band, e.g. for 3rd order <img src="https://render.githubusercontent.com/render/math?math=f_{3,k}=F(x_k^3)_k">
1. Correlate the distortion with the original data <img src="https://render.githubusercontent.com/render/math?math=C_k = \frac {\Sigma f_{3,k}^{*}  f_k } {\Sigma f_{3,k}^*  f_{3,k} }"> over some timespan.
1. Substract the correlated distortions <img src="https://render.githubusercontent.com/render/math?math=f_{k,\textrm{corrected}}=  f_k - C_k  f_{3,k}">

 
# Fast implementation
- Use [Modified DFT filter banks with perfect reconstruction](https://ieeexplore.ieee.org/document/803480) (MDFT)
- Assume that the RFI is much larger than noise level, so for bands without RFI, <img src="https://render.githubusercontent.com/render/math?math={F^{-1}(f_k)} << |x|">, so <img src="https://render.githubusercontent.com/render/math?math=x_k^3 = x^2_k x_k^* \approx x^3 -2x^2F^{-1*}(f_k)">.  The first term required one MDFT transformation  and the second term can be calculated without any MDFT by calculating the convolution with the filter coefficiens twice. 
- The IMD cancelation only require one additional MDFT and a convolution using order  <img src="https://render.githubusercontent.com/render/math?math=P^2N"> multiplications, where _N_ is the number of frequency bands and _P_ is the number of taps per band.  

# Example
The figure below shows the measured output of a receiver when a strong CW signal (S1 @ 155MHz) and W-CDMA signal (S2 @ 1178MHz) is injected. In order to see the intermodulation products better, the noise floor is lowered by calculation the cross-correlation between the X and Y polarisation. This does not reduce the RFI or intermodulation distortion, as they are common between the two polarisation.

Note the various 3rd order intermodulation products and the resulting clean spectrum after the distortion is substracted from both the X and Y raw data. After cancelation, the spurious-free dynamic range due to 3rd order intermodulation improved by more than 20dB and is well below the noise level of a single polarisation.  
![plot](XYcrosscorr2.png)

